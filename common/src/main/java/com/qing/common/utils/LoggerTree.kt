package com.qing.common.utils

import com.orhanobut.logger.AndroidLogAdapter
import com.orhanobut.logger.FormatStrategy
import com.orhanobut.logger.Logger
import com.orhanobut.logger.PrettyFormatStrategy
import timber.log.Timber

/**
@author 李燕青,
@Email 18476618068@163.com,
@Date  2022/10/23.
 */
class LoggerTree(private val usePretty: Boolean = true) : Timber.DebugTree() {

    init {
        if (usePretty) {
            val formatStrategy: FormatStrategy = PrettyFormatStrategy.newBuilder()
                .methodCount(1)
                .showThreadInfo(false)
                .tag("LoggerTree")
                .methodOffset(4)
                .build()
            Logger.addLogAdapter(AndroidLogAdapter(formatStrategy))
        }
    }

    override fun log(
        priority: Int,
        tag: String?,
        message: String,
        t: Throwable?
    ) {
        if (usePretty) {
            Logger.log(priority, tag, message, t)
        } else {
            super.log(priority, tag, message, t)
        }
    }
}