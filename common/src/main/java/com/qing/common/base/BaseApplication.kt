package com.qing.common.base

import android.app.Application
import com.qing.common.ActivityManager
import com.qing.common.utils.LoggerTree
import timber.log.Timber

/**
@author 李燕青,
@Email 18476618068@163.com,
@Date  2022/10/23.
 */
class  BaseApplication : Application(){
    companion object{
        lateinit var application: Application

        private val activityManager = ActivityManager()

        fun getActivityManager():ActivityManager{
            return activityManager
        }
    }

    override fun onCreate() {
        super.onCreate()
        application = this
        Timber.plant(LoggerTree())
    }
}