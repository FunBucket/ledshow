package com.qing.common.base

import android.os.Bundle
import androidx.viewbinding.ViewBinding

/**
@author 李燕青,
@Email 18476618068@163.com,
@Date  2022/10/23.
 */
interface IUiCallback {
    fun initBeforeView(savedInstanceState: Bundle?)

    //初始化视图
    fun initData(savedInstanceState: Bundle?)

    //获取布局Id
    fun getBinding(): ViewBinding
}