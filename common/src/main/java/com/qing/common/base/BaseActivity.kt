package com.qing.common.base

import android.app.Activity
import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.Toast
import android.widget.Toolbar
import androidx.appcompat.app.AppCompatActivity
import androidx.viewbinding.ViewBinding
import com.qing.common.R
import timber.log.Timber
import java.lang.reflect.Method
import java.lang.reflect.ParameterizedType
import java.util.*


/**
 * 基类Activity，普通Activity继承即可
@author 李燕青,
@Email 18476618068@163.com,
@Date  2022/10/23.
 */
abstract class BaseActivity<B : ViewBinding> : AppCompatActivity(), IUiCallback {
    //Activity 上下文
    protected lateinit var context: Activity

    //试图绑定
    protected lateinit var mBinding: B

    //弹窗
    private var mDialog: Dialog? = null

    companion object {
        private const val FAST_CLICK_DELAY_TIME = 500
        private var lastClickTime = 0
        private  val mTAG = this::class.java.simpleName
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //绑定视图
        initBeforeView(savedInstanceState)
        this.context = this
        //获取Activity的上下文
        BaseApplication.getActivityManager().addActivity(this)

        mBinding = getBinding()

        setContentView(mBinding.root)

        initData(savedInstanceState)
    }

    /**
     * Toast消息提示  字符
     * @param msg 字符
     */
    protected fun showToast(msg: CharSequence) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
    }

    /**
     * Toast消息提示  资源ID
     * @param resourceId
     */
    protected fun showToast(resourceId: Int) {
        Toast.makeText(context, resourceId, Toast.LENGTH_SHORT).show()
    }

    /**
     * 弹窗出现
     */
    protected fun showLoadingDialog() {
        if (mDialog == null) {
            mDialog = Dialog(context, R.style.loading_dialog)
        }
        mDialog?.apply {
            setContentView(R.layout.dialog_loading)
            setCancelable(false)
            Objects.requireNonNull(this.window?.setBackgroundDrawableResource(android.R.color.transparent))
            show()
        }
    }

    /**
     * 弹窗隐藏
     */
    protected fun hideLoadingDialog() {
        if (mDialog != null) {
            mDialog!!.dismiss()
        }
        mDialog = null
    }

    /**
     * 返回 不需要参数
     */
    protected fun Back() {
        context.finish()
        if (!isFastClick()) {
            context.finish();
        }
    }

    /**
     * 返回 toolbar控件点击
     *
     * @param toolbar
     */
    protected fun Back(toolbar: Toolbar) {
        toolbar.setNavigationOnClickListener {
            context.finish()
            if (!isFastClick()) {
                context.finish();
            }
        }
    }

    /**
     * 两次点击间隔不能少于500ms
     *
     * @return flag
     */
    protected open fun isFastClick(): Boolean {
        var flag = true
        val currentClickTime = System.currentTimeMillis()
        if (currentClickTime - lastClickTime >= FAST_CLICK_DELAY_TIME) {
            flag = false
        }
        lastClickTime = currentClickTime.toInt()
        return flag
    }

    override fun getBinding(): B {
        var binding:B? = null
        try {
            val superclass = javaClass.genericSuperclass as ParameterizedType
            val aClass = superclass.actualTypeArguments[2] as Class<*>
            val method: Method = aClass.getDeclaredMethod("inflate", LayoutInflater::class.java)
            binding = method.invoke(null, layoutInflater) as B
        } catch (e: Exception) {
            Timber.tag("BaseFragment").d(e)
        }
        return  binding!!
    }
}