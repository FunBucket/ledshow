package com.qing.common.base

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding
import timber.log.Timber
import java.lang.reflect.Method
import java.lang.reflect.ParameterizedType
import java.sql.Time

/**
@author 李燕青,
@Email 18476618068@163.com,
@Date  2022/10/23.
 */
abstract class BaseFragment<B : ViewBinding> : Fragment(), IUiCallback {
    protected lateinit var mLayoutInflater: LayoutInflater
    protected var context: Activity? = null

    //试图绑定
    protected lateinit var mBinding: B

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initBeforeView(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mLayoutInflater = layoutInflater
        mBinding = getBinding()
        return mBinding.root
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initData(savedInstanceState)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is Activity) {
            this.context = context as Activity
        }
    }

    override fun onDetach() {
        super.onDetach()
        this.context = null
    }

    override fun initBeforeView(savedInstanceState: Bundle?) {
    }

    override fun getBinding(): B {
        var binding: B? = null
        try {
            val superclass = javaClass.genericSuperclass as ParameterizedType
            val aClass = superclass.actualTypeArguments[2] as Class<*>
            val method: Method = aClass.getDeclaredMethod("inflate", LayoutInflater::class.java)
            binding = method.invoke(null, layoutInflater) as B
        } catch (e: Exception) {
            Timber.tag("BaseFragment").e(e)
        }
        return binding!!
    }
}