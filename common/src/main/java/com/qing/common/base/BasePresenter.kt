package com.qing.common.base

import java.lang.ref.WeakReference

/**
 * Presenter基类 操作视图View
 *
@author 李燕青,
@Email 18476618068@163.com,
@Date  2022/10/23.
 */
open class BasePresenter<V : BaseView> {
    //弱引用View
    protected var mWeakReference: WeakReference<V>? = null
    private var mView: V? = null

    /**
     * 绑定View
     *
     * @param view
     */
    fun attachView(view: V) {
        mView = view
        mWeakReference = WeakReference<V>(mView)
    }

    /**
     * 解绑View
     */
    fun detachView() {
        mView = null
        mWeakReference?.apply {
            this.clear()
        }
        mWeakReference = null
    }

    /**
     * 获取view
     *
     * @return
     */
    fun getView(): V? {
        mWeakReference?.apply {
            return this.get()
        }
        return null
    }


    /**
     * View是否绑定
     *
     * @return
     */
    fun isViewAttached(): Boolean {
        return mView == null
    }
}