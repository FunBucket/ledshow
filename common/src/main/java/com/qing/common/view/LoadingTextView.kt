package com.qing.common.view

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatTextView

/**
@author 李燕青,
@Email 18476618068@163.com,
@Date  2022/10/23.
 */
class LoadingTextView : AppCompatTextView {
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    )

    private var mLinearGradient: LinearGradient? = null
    private var mGradientMatrix: Matrix? = null
    private var mPaint: Paint? = null
    private var mViewWidth = 0
    private var mTranslate = 0

    private val mAnimating = true


    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        if (mViewWidth == 0) {
            mViewWidth = measuredWidth
            mPaint = Paint()
            mLinearGradient = LinearGradient(
                -mViewWidth.toFloat(), 0f, 0f, 0f, intArrayOf(
                    0x33ffffff,
                    0xffd81e06.toInt(), 0x33ffffff
                ),
                floatArrayOf(0f, 0.5f, 1f), Shader.TileMode.CLAMP
            )
            mPaint!!.shader = mLinearGradient
            mGradientMatrix = Matrix()
        }
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        if (mAnimating && mLinearGradient != null) {
            mTranslate += mViewWidth / 10
            if (mTranslate > 2 * mViewWidth) {
                mTranslate -= mViewWidth
            }
            mGradientMatrix?.setTranslate(mTranslate.toFloat(), 0f)
            mLinearGradient?.setLocalMatrix(mGradientMatrix)
            postInvalidateDelayed(20)
        }
    }
}