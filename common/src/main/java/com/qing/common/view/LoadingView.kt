package com.qing.common.view

import android.content.Context
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatImageView
import com.qing.common.R
import java.lang.ref.SoftReference

/**
 * 加载框
@author 李燕青,
@Email 18476618068@163.com,
@Date  2022/10/23.
 */
class LoadingView : AppCompatImageView {
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    )

    private var mCenterRotateX = 0f //图片旋转点x
    private var mCenterRotateY = 0f//图片旋转点y
    private var mRunnable: LoadingRunnable? = null


    init {
        scaleType = ScaleType.MATRIX
        val bitmap = BitmapFactory.decodeResource(resources, R.drawable.icon_loading)
        setImageBitmap(bitmap)
        mCenterRotateX = bitmap.width / 2.toFloat()
        mCenterRotateY = bitmap.height / 2.toFloat()
    }

    /**
     * onDraw()之前调用
     */
    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        if (mRunnable == null) {
            mRunnable = LoadingRunnable(this)
        }
        mRunnable?.apply {
            if (!isLoading) {
                start()
            }
        }
    }

    /**
     * view销毁时调用
     */
    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        mRunnable?.apply {
            stop()
        }
        mRunnable = null
    }

    inner class LoadingRunnable(loadingView: LoadingView) : Runnable {

        var isLoading = false
        private var mMatrix: Matrix? = null
        private var mLoadingViewSoftReference: SoftReference<LoadingView>? = null
        private var mDegrees = 0f

        init {
            mLoadingViewSoftReference = SoftReference(loadingView)
            mMatrix = Matrix()
        }

        override fun run() {
            if (mLoadingViewSoftReference?.get()?.mRunnable != null && mMatrix != null) {
                mDegrees += 30f
                mMatrix!!.setRotate(mDegrees, mCenterRotateX, mCenterRotateY)
            }
            mLoadingViewSoftReference?.get()?.imageMatrix = mMatrix
            if (mDegrees == 360f) {
                mDegrees = 0f
            }
            if (isLoading) {
                mLoadingViewSoftReference?.apply {
                    get()?.postDelayed(this.get()?.mRunnable, 100)
                }
            }
        }

        fun stop() {
            isLoading = false
        }

        fun start() {
            isLoading = true
            if (mMatrix != null && mLoadingViewSoftReference?.get()?.mRunnable != null) {
                mLoadingViewSoftReference?.apply {
                    get()?.postDelayed(this.get()?.mRunnable, 100)
                }
            }
        }

    }

}