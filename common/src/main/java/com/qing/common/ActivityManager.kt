package com.qing.common

import android.app.Activity

/**
 * 管理創建的activity
 *
@author 李燕青,
@Email 18476618068@163.com,
@Date  2022/10/23.
 */
class ActivityManager {
    //保存所有创建的Activity
    private var activityList: MutableList<Activity> = ArrayList()
    /**
     * 添加Activity
     * @param activity
     */
    fun addActivity(activity: Activity?) {
        activity?.apply {
            activityList.add(this)
        }
    }

    /**
     * 移除Activity
     * @param activity
     */
    fun removeActivity(activity: Activity?) {
        activity?.apply {
            activityList.remove(activity)
        }
    }

    /**
     * 关闭所有Activity
     */
    fun closeAllActivity() {
        for (activity in activityList) {
            activity.finish()
        }
    }
}