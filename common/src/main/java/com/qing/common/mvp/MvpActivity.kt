package com.qing.common.mvp

import android.os.Bundle
import androidx.viewbinding.ViewBinding
import com.qing.common.base.BaseActivity
import com.qing.common.base.BasePresenter
import com.qing.common.base.BaseView


/**
@author 李燕青,
@Email 18476618068@163.com,
@Date  2022/10/23.
 */
abstract class MvpActivity<V:BaseView,P : BasePresenter<V>,B:ViewBinding> : BaseActivity<B>() {

    protected var mPresenter: P? = null

    /**
     * 创建Presenter
     */
    protected abstract fun createPresenter(): P
    override fun initBeforeView(savedInstanceState: Bundle?) {
        //创建
        mPresenter = createPresenter()
        //绑定View
        mPresenter!!.attachView(this as V)
    }

    /**
     * 页面销毁时解除绑定
     */
    override fun onDestroy() {
        super.onDestroy()
        mPresenter!!.detachView()
    }
}
