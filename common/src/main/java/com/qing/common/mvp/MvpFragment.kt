package com.qing.common.mvp

import android.os.Bundle
import androidx.viewbinding.ViewBinding
import com.qing.common.base.BaseFragment
import com.qing.common.base.BasePresenter
import com.qing.common.base.BaseView


/**
@author 李燕青,
@Email 18476618068@163.com,
@Date  2022/10/23.
 */
abstract class MvpFragment<V:BaseView,P : BasePresenter<V>,B:ViewBinding> : BaseFragment<B>() {
 protected lateinit var mPresenter:P

 /**
  * 创建Presenter
  */
 protected abstract fun createPresent(): P
 override fun initBeforeView(savedInstanceState: Bundle?) {
  mPresenter = createPresent()
  mPresenter.attachView(this as V)
 }

 override fun onDestroy() {
  super.onDestroy()
  mPresenter.detachView()
 }
}
