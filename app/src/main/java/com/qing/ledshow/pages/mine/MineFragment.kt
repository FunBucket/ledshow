package com.qing.ledshow.pages.mine

import android.os.Bundle
import com.qing.common.mvp.MvpFragment
import com.qing.ledshow.databinding.FragmentScreenBinding
import com.qing.ledshow.pages.home.presenter.TaskPresenter
import com.qing.ledshow.pages.home.view.MineView
import com.qing.ledshow.pages.home.view.TaskView


class MineFragment : MvpFragment<TaskView, TaskPresenter, FragmentScreenBinding>(),MineView {
    override fun initData(savedInstanceState: Bundle?) {

    }

    override fun createPresent(): TaskPresenter {
        return TaskPresenter()
    }

}