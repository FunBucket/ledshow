package com.qing.ledshow.pages.home.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.qing.ledshow.pages.material.MaterialFragment
import com.qing.ledshow.pages.mine.MineFragment
import com.qing.ledshow.pages.program.ProgramFragment
import com.qing.ledshow.pages.screen.ScreenFragment
import com.qing.ledshow.pages.task.TaskFragment

/**
@author 李燕青,
@Email 18476618068@163.com,
@Date  2022/10/26.
 */
class IndexFragmentAdapter(fragmentManager: FragmentManager, lifecycle: Lifecycle) :
    FragmentStateAdapter(fragmentManager, lifecycle) {

    private val fragments = listOf<Fragment>(
        ScreenFragment(),
        ProgramFragment(),
        MaterialFragment(),
        TaskFragment(),
        MineFragment()
    )

    override fun getItemCount(): Int {
        return fragments.size
    }

    override fun createFragment(position: Int): Fragment {
        return fragments[position]
    }
}