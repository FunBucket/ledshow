package com.qing.ledshow.pages.material

import android.os.Bundle
import com.qing.common.mvp.MvpFragment
import com.qing.ledshow.databinding.FragmentScreenBinding
import com.qing.ledshow.pages.home.presenter.TaskPresenter
import com.qing.ledshow.pages.home.view.MaterialView
import com.qing.ledshow.pages.home.view.TaskView


class MaterialFragment : MvpFragment<TaskView, TaskPresenter, FragmentScreenBinding>(),MaterialView {
    override fun initData(savedInstanceState: Bundle?) {

    }

    override fun createPresent(): TaskPresenter {
        return TaskPresenter()
    }

}