package com.qing.ledshow.pages.program

import android.os.Bundle
import com.qing.common.mvp.MvpFragment
import com.qing.ledshow.databinding.FragmentScreenBinding
import com.qing.ledshow.pages.home.presenter.TaskPresenter
import com.qing.ledshow.pages.home.view.ProgramView
import com.qing.ledshow.pages.home.view.TaskView


class ProgramFragment : MvpFragment<TaskView, TaskPresenter, FragmentScreenBinding>(),ProgramView {
    override fun initData(savedInstanceState: Bundle?) {

    }

    override fun createPresent(): TaskPresenter {
        return TaskPresenter()
    }

}