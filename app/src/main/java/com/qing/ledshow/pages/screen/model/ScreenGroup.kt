package com.qing.ledshow.pages.screen.model

/**
@author 李燕青,
@Email 18476618068@163.com,
@Date  2022/11/6.
 */
data class ScreenGroup(
    val groupName: String,
    val list: List<Screen>
)