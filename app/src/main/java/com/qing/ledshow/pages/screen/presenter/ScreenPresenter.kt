package com.qing.ledshow.pages.screen.presenter

import com.qing.common.base.BasePresenter
import com.qing.ledshow.R
import com.qing.ledshow.pages.home.view.ScreenView
import com.qing.ledshow.pages.screen.model.Screen
import com.qing.ledshow.pages.screen.model.ScreenGroup
import com.qing.ledshow.pages.screen.model.ScreenOperate
import timber.log.Timber

/**
@author 李燕青,
@Email 18476618068@163.com,
@Date  2022/10/24.
 */
class ScreenPresenter : BasePresenter<ScreenView>() {
    val groupList = mutableListOf<ScreenGroup>()
    val screenOperates = listOf<ScreenOperate>(
        ScreenOperate("开机", R.drawable.icon_launcher),
        ScreenOperate("关机", R.drawable.icon_launcher),
        ScreenOperate("亮度", R.drawable.icon_launcher),
        ScreenOperate("删除", R.drawable.icon_launcher),
        ScreenOperate("重置任务", R.drawable.icon_launcher),
        ScreenOperate("重启控制器", R.drawable.icon_launcher),
        ScreenOperate("检查固件", R.drawable.icon_launcher),
        ScreenOperate("升级固件", R.drawable.icon_launcher),
        )

    companion object {
        private const val TAG = "ScreenPresenter"
    }

    init {
        Timber.tag(TAG).d(groupList.isEmpty().toString())
        groupList.add(
            ScreenGroup(
                "上海仰邦科技",
                listOf(
                    Screen(
                        "上海市徐汇区钦州北路1199号",
                        "",
                        0,
                        false,
                        "上海仰邦科技",
                        96,
                        0,
                        "",
                        "",
                        "这是一个屏幕名称",
                        "",
                        0,
                        "",
                        384
                    ),
                    Screen(
                        "上海市徐汇区钦州北路1199号",
                        "",
                        0,
                        false,
                        "上海仰邦科技",
                        96,
                        0,
                        "",
                        "",
                        "这是一个屏幕名称",
                        "",
                        0,
                        "",
                        384
                    ),
                )
            )
        )
        groupList.add(
            ScreenGroup(
                "上海仰邦科技",
                listOf(
                    Screen(
                        "上海市徐汇区钦州北路1199号",
                        "",
                        0,
                        false,
                        "上海仰邦科技",
                        96,
                        0,
                        "",
                        "",
                        "这是一个屏幕名称",
                        "",
                        0,
                        "",
                        384
                    ),
                    Screen(
                        "上海市徐汇区钦州北路1199号",
                        "",
                        0,
                        false,
                        "上海仰邦科技",
                        96,
                        0,
                        "",
                        "",
                        "这是一个屏幕名称",
                        "",
                        0,
                        "",
                        384
                    ),
                )
            )
        )
        groupList.add(
            ScreenGroup(
                "上海仰邦科技",
                listOf(
                    Screen(
                        "上海市徐汇区钦州北路1199号",
                        "",
                        0,
                        false,
                        "上海仰邦科技",
                        96,
                        0,
                        "",
                        "",
                        "这是一个屏幕名称",
                        "",
                        0,
                        "",
                        384
                    ),
                    Screen(
                        "上海市徐汇区钦州北路1199号",
                        "",
                        0,
                        false,
                        "上海仰邦科技",
                        96,
                        0,
                        "",
                        "",
                        "这是一个屏幕名称",
                        "",
                        0,
                        "",
                        384
                    ),
                )
            )
        )
    }
}