package com.qing.ledshow.pages.screen.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.qing.ledshow.databinding.ItemScreenOperateBinding
import com.qing.ledshow.pages.screen.model.ScreenOperate

class ScreenOperationAdapter(private val context: Context, private val list: List<ScreenOperate>) :
    RecyclerView.Adapter<ScreenOperationAdapter.ScreenOperationViewHolder>() {


    inner class ScreenOperationViewHolder(private val binding: ItemScreenOperateBinding) :
        ViewHolder(binding.root) {

        fun bind(screenOperate: ScreenOperate) {
            binding.tvOperateText.text = screenOperate.name
            binding.vOperateIcon.background = ContextCompat.getDrawable(context, screenOperate.icon)

            binding.root.setOnClickListener {

            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ScreenOperationViewHolder {
        return ScreenOperationViewHolder(
            ItemScreenOperateBinding.inflate(
                LayoutInflater.from(
                    context
                )
            )
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ScreenOperationViewHolder, position: Int) {
        holder.bind(list[position])
    }
}