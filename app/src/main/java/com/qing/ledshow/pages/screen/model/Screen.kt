package com.qing.ledshow.pages.screen.model

data class Screen(
    val address: String,
    val addressIds: String,
    val cardId: Int,
    val checked: Boolean,
    val groupName: String,
    val height: Int,
    val id: Int,
    val locationType: String,
    val modelName: String,
    val name: String,
    val sceneType: String,
    val status: Int,
    val vehiclePosition: String,
    val width: Int
)