package com.qing.ledshow.pages.screen.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import androidx.core.content.ContextCompat
import com.qing.ledshow.R
import com.qing.ledshow.databinding.ItemScreenBinding
import com.qing.ledshow.databinding.ItemScreenGroupBinding
import com.qing.ledshow.pages.screen.model.ScreenGroup
import timber.log.Timber
import java.text.FieldPosition

class ExpandedListAdapter(
    private val context: Context,
    private val screenGroupList: List<ScreenGroup>
) :
    BaseExpandableListAdapter() {

    private var groupBindings = mutableListOf<ItemScreenGroupBinding>()

    companion object {
        private const val TAG = "ExpandedListAdapter"
    }

    override fun getGroupCount(): Int {
        return screenGroupList.size
    }

    override fun getChildrenCount(groupPosition: Int): Int {
        return screenGroupList[groupPosition].list.size
    }

    override fun getGroup(groupPosition: Int): Any {
        return screenGroupList[groupPosition]
    }

    override fun getChild(groupPosition: Int, childPosition: Int): Any {
        return screenGroupList[groupPosition].list[childPosition]
    }

    override fun getGroupId(groupPosition: Int): Long {
        return groupPosition.toLong()
    }

    override fun getChildId(groupPosition: Int, childPosition: Int): Long {
        return childPosition.toLong()
    }

    override fun hasStableIds(): Boolean {
        return false
    }

    override fun getGroupView(
        groupPosition: Int,
        isExpanded: Boolean,
        convertView: View?,
        parent: ViewGroup?
    ): View {
        val binding =
            parent?.let { ItemScreenGroupBinding.inflate(LayoutInflater.from(it.context)) }
        binding?.apply {
            groupBindings.add(groupPosition, this)
            tvGroupName.text = screenGroupList[groupPosition].groupName
            var totalScreen = screenGroupList[groupPosition].list.size
            var onLineScreen = 0
            screenGroupList[groupPosition].list.forEach { screen ->
                if (screen.status == 1) {
                    onLineScreen++
                }
            }
            tvGroupScreenNumber.text = "($onLineScreen/$totalScreen)"
            binding.flContentRoot.isSelected = isExpanded
        }
        return binding!!.root
    }

    override fun getChildView(
        groupPosition: Int,
        childPosition: Int,
        isLastChild: Boolean,
        convertView: View?,
        parent: ViewGroup?
    ): View {
        val binding =
            parent?.let { ItemScreenBinding.inflate(LayoutInflater.from(it.context)) }
        binding?.apply {
            val screen = screenGroupList[groupPosition].list[childPosition]
            cbSelectCheckbox.onMyClickListener = {

            }
            vScreenState.isActivated = screen.status == 1
            vScreenLocation.isActivated = screen.status == 1
            tvScreenName.text = screen.name
            tvScreenAddress.text = screen.address
            tvScreenNumberAndSize.text = "${screen.id} | ${screen.width}*${screen.height}"
            binding.llContentRoot.background = ContextCompat.getDrawable(
                context, if (isLastChild) {
                    R.drawable.bottom_conner_white_bg
                } else {
                    R.drawable.list_item_bg
                }
            )
        }
        return binding!!.root
    }

    override fun isChildSelectable(groupPosition: Int, childPosition: Int): Boolean {
        return true
    }
}