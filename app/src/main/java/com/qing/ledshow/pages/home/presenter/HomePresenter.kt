package com.qing.ledshow.pages.home.presenter

import com.qing.common.base.BasePresenter
import com.qing.ledshow.pages.home.view.TaskView

/**
@author 李燕青,
@Email 18476618068@163.com,
@Date  2022/10/24.
 */
class HomePresenter: BasePresenter<TaskView>() {

}