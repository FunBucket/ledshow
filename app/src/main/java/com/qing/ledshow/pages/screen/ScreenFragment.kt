package com.qing.ledshow.pages.screen

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.qing.common.mvp.MvpFragment
import com.qing.ledshow.databinding.FragmentScreenBinding
import com.qing.ledshow.pages.home.view.ScreenView
import com.qing.ledshow.pages.screen.adapter.ExpandedListAdapter
import com.qing.ledshow.pages.screen.adapter.ScreenOperationAdapter
import com.qing.ledshow.pages.screen.presenter.ScreenPresenter


class ScreenFragment : MvpFragment<ScreenView, ScreenPresenter, FragmentScreenBinding>(),
    ScreenView {


    override fun initData(savedInstanceState: Bundle?) {
        mBinding.pbPageBar.apply {
            leadingClick = {
            }
            actionClick = {}
        }

        if (mPresenter.groupList.isEmpty()) {
            mBinding.apply {
                llEmptyScreen.visibility = View.VISIBLE
                llScreenView.visibility = View.GONE
            }
        } else {
            mBinding.apply {
                llEmptyScreen.visibility = View.GONE
                llScreenView.visibility = View.VISIBLE
            }
        }

        mBinding.elScreenGroup.apply {
            val adapter = ExpandedListAdapter(context, mPresenter.groupList)
            setAdapter(adapter)
        }

        mBinding.rlScreenList.apply {
            layoutManager = LinearLayoutManager(context).apply {
                orientation = LinearLayoutManager.HORIZONTAL
            }
            adapter = ScreenOperationAdapter(context,mPresenter.screenOperates)
        }
    }

    override fun createPresent(): ScreenPresenter {
        return ScreenPresenter()
    }

}