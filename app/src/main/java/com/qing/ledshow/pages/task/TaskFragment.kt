package com.qing.ledshow.pages.task

import android.os.Bundle
import com.qing.common.mvp.MvpFragment
import com.qing.ledshow.databinding.FragmentScreenBinding
import com.qing.ledshow.pages.home.presenter.TaskPresenter
import com.qing.ledshow.pages.home.view.TaskView


class TaskFragment : MvpFragment<TaskView, TaskPresenter, FragmentScreenBinding>(),TaskView {

    override fun initData(savedInstanceState: Bundle?) {

    }

    override fun createPresent(): TaskPresenter {
        return TaskPresenter()
    }

}