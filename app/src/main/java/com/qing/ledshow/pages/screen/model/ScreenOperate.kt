package com.qing.ledshow.pages.screen.model

data class ScreenOperate(
    val name:String,
    val icon:Int
)
