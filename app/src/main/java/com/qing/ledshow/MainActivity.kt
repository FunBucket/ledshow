package com.qing.ledshow

import android.os.Bundle
import android.util.Log
import android.view.View
import com.qing.common.mvp.MvpActivity
import com.qing.ledshow.databinding.ActivityMainBinding
import com.qing.ledshow.pages.home.adapter.IndexFragmentAdapter
import com.qing.ledshow.pages.home.presenter.TaskPresenter
import com.qing.ledshow.pages.home.view.TaskView

class MainActivity : MvpActivity<TaskView, TaskPresenter, ActivityMainBinding>(), TaskView {
    private val buttons = mutableListOf<View>()

    companion object{
        private const val TAG = "MainActivity"
    }

    override fun initData(savedInstanceState: Bundle?) {
        mBinding.homePager.apply {
            adapter = IndexFragmentAdapter(supportFragmentManager,lifecycle)
        }
        buttons.apply {
            add(mBinding.llScreenItem)
            add(mBinding.llProgramItem)
            add(mBinding.llMaterialItem)
            add(mBinding.llTaskItem)
            add(mBinding.llMineItem)
        }
        mBinding.llScreenItem.isSelected = true
        mBinding.homePager.apply {
            currentItem = 0
            isUserInputEnabled = false
        }
        initListener()
    }
    private fun initListener(){
        mBinding.llScreenItem.setOnClickListener {
            changePage(0)
        }
        mBinding.llProgramItem.setOnClickListener {
            changePage(1)
        }
        mBinding.llMaterialItem.setOnClickListener {
            changePage(2)
        }
        mBinding.llTaskItem.setOnClickListener {
            changePage(3)
        }
        mBinding.llMineItem.setOnClickListener {
            changePage(4)
        }
    }

    private fun changePage(index: Int) {
        for (i in 0 until buttons.size){
            buttons[i].isSelected = (i == index)
        }
        mBinding.homePager.currentItem = index
    }


    override fun createPresenter(): TaskPresenter {
       return  TaskPresenter()
    }



}