package com.qing.ledshow.widgets

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View


class CustomCheckBox : View {
    constructor(ctx: Context) : super(ctx)
    constructor(ctx: Context, attrs: AttributeSet) : super(ctx, attrs)
    constructor(ctx: Context, attrs: AttributeSet, attrType: Int) : super(ctx, attrs, attrType)

    companion object

    var onMyClickListener: ((Boolean) -> Any)? = null

    private var isCheck = false

    private var canvas: Canvas? = null


    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        this.canvas = canvas
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        val action = event?.action
        action?.apply {
            when (this) {
                MotionEvent.ACTION_DOWN -> {
                    this@CustomCheckBox.isSelected = !this@CustomCheckBox.isSelected
                    onMyClickListener?.invoke(isCheck)
                }
                else -> {}
            }
        }
        return super.onTouchEvent(event)
    }


}