package com.qing.ledshow.widgets

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import androidx.constraintlayout.widget.ConstraintLayout
import com.qing.ledshow.R
import com.qing.ledshow.databinding.SearchBarBinding
import timber.log.Timber

/**
@author 李燕青,
@Email 18476618068@163.com,
@Date  2022/10/31.
 */
class SearchBar(private val ctx:Context, private val attr:AttributeSet) : ConstraintLayout(ctx,attr) {
    private var binding:SearchBarBinding
    val onEditListener:((text:String)-> Unit?)?=null

    init {
        val view = LayoutInflater.from(ctx).inflate(R.layout.search_bar, this, true)
        binding = SearchBarBinding.bind(view)
        initData()
        initListener()
    }

    companion object{
        private const val TAG = "SearchBar"
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
//        binding.cbSelectCheckbox.post{
//            Timber.tag(TAG).d(binding.cbSelectCheckbox.measuredHeight.toString())
//            Timber.tag(TAG).d(binding.cbSelectCheckbox.measuredWidth.toString())
//        }
    }

    private fun initListener() {
        binding.etSearchBar.addTextChangedListener(object : TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                Timber.tag(TAG).d(s.toString().trim())
                onEditListener?.let { it(s.toString().trim()) }
            }

        })
    }

    private fun initData() {
        val typeArray = this.context.obtainStyledAttributes(attr,R.styleable.PageBar)
        val type = typeArray.getInt(R.styleable.SearchBar_side_action,0)
        when (type) {
            0 -> {
                binding.mcbCheckBox.onMyClickListener = {
                    Timber.tag(TAG).d(it.toString())
                }
            }
            else -> {}
        }
    }
}