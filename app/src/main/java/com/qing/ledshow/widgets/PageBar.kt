package com.qing.ledshow.widgets

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import com.qing.ledshow.R
import com.qing.ledshow.databinding.PageBarBinding

/**
@author 李燕青,
@Email 18476618068@163.com,
@Date  2022/10/27.
 */
class PageBar (ctx: Context, private val attrs: AttributeSet): ConstraintLayout(ctx, attrs) {

    private var title = ""
    private var leading: Drawable? = null
    private var action: Drawable? = null
    var leadingClick: (() -> Unit?)? = null
    var actionClick: (() -> Unit?)? = null

    companion object{
        private const val TAG = "PageBar"
    }

    private var binding: PageBarBinding

    init {
        val view:View = LayoutInflater.from(ctx).inflate(R.layout.page_bar,this,true)
        binding = PageBarBinding.bind(view)
        initData()
        initListener()
    }

    private fun initListener() {
        binding.vLeading.setOnClickListener {
            leadingClick?.apply {
                this.invoke()
            }
        }
        binding.vAction.setOnClickListener {
            actionClick?.apply {
                this.invoke()
            }
        }
    }

    private fun initData() {
        val typeArray = this.context.obtainStyledAttributes(attrs,R.styleable.PageBar)
        title = typeArray.getString(R.styleable.PageBar_title).let {
            it ?: ""
        }
        leading = typeArray.getDrawable(R.styleable.PageBar_leading)
        action = typeArray.getDrawable(R.styleable.PageBar_action)
        Log.d(TAG, "initData: $leading")
        binding.tvBarTitle.text = title
        leading?.apply {
            binding.vLeading.background = this }
        action?.apply {
            binding.vAction.background = this }
    }

}